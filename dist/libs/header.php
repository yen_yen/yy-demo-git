<!-- Google Tag Manager -->
<!-- End Google Tag Manager -->

<header class="header">
	<div class="inHeader">
		<h1 id="logo"><a href="<?php echo APP_URL ?>"><img src="<?php echo APP_ASSETS ?>img/common/logo.svg" alt="logo"></a></h1>
		<p class="hamberger"><span class="ham"> </span><span class="ber"> </span><span class="ger"> </span></p>
		<div class="layerMenu">
			<div class="inLayer">
				<div class="hNavi">
					<ul>
						<li class="calling"><a href="tel:0000-000-000"><span class="make">タップで電話をかける</span><span class="tel">0000-000-000</span><span class="time">受付時間 9:00〜18:00 (土日祝定休)</span></a></li>
					</ul>
				</div>
				<ul class="gNavi">
					<li class="gHome"><a href="#"><span>トップページ</span></a></li>
					<li class="gWorks hasSub"><a href="#"><span>施工事例</span></a><em class="plus"> </em>
						<div class="navSub">
							<ul>
								<li><a href="#">テキストテキスト</a></li>
								<li><a href="#">テキストテキスト</a></li>
								<li><a href="#">テキストテキストテキスト</a></li>
								<li><a href="#">テキストテキストテキスト</a></li>
							</ul>
							<p class="closeSub">Close</p>
						</div>
					</li>
					<li class="gAbout hasSub"><a href="#"><span>トスコの家づくり</span></a><em class="plus"> </em>
						<div class="navSub">
							<ul>
								<li><a href="#"><span>デザイン</span></a></li>
								<li><a href="#"><span>ヒアリング</span></a></li>
								<li><a href="#"><span>ご要望に合わせた家づくり</span></a></li>
								<li><a href="#"><span>アフターサポート</span></a></li>
								<li><a href="#"><span>家づくりの流れ</span></a></li>
							</ul>
							<p class="closeSub">Close</p>
						</div>
					</li>
					<li class="gVoice"><a href="#"><span>お客様の声</span></a></li>
					<li class="gRelease"><a href="#"><span>土地情報</span></a></li>
					<li class="gContact"><a href="#"><span>お問い合わせ</span></a></li>
				</ul>
				<p class="close_layer"><span>× 閉じる</span></p>
			</div>
		</div>
	</div>
</header>