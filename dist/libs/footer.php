<footer id="footer">
	<div class="footerInner clearfix">
		<ul class="col1">
			<li><a href="javascript:void(0)" rel="<?php echo APP_URL ?>">トップページ</a></li>
			<li><a href="<?php echo APP_URL ?>">トスコの家づくり</a>
				<ul>
					<li><a href="<?php echo APP_URL ?>">デザイン</a></li>
					<li><a href="<?php echo APP_URL ?>">ヒアリング</a></li>
					<li><a href="<?php echo APP_URL ?>">ご要望に合わせた<br class="tab">家づくり</a></li>
				</ul>
			</li>
		</ul>
		<ul class="col2">
			<!--<li><a href="<?php echo APP_URL ?>">価格</a></li>
			<li><a href="<?php echo APP_URL ?>">機能</a></li>-->
			<li><a href="<?php echo APP_URL ?>">アフター<br class="tab">サポート</a></li>
			<li><a href="<?php echo APP_URL ?>">家づくりの流れ</a></li>
			<li><a href="<?php echo APP_URL ?>">施工事例</a></li>
			<li><a href="<?php echo APP_URL ?>">お客様の<br class="tab">喜びの声</a></li>
			<li><a href="<?php echo APP_URL ?>">物件情報</a></li>
		</ul>
		<ul class="col3">
			<li><a href="<?php echo APP_URL ?>">物件資料請求・<br class="tab">お問い合わせ</a></li>
			<li><a href="<?php echo APP_URL ?>">ニュース・イベント</a></li>
			<li><a href="<?php echo APP_URL ?>">ブログ</a></li>
			<li><a href="<?php echo APP_URL ?>">会社案内</a></li>
			<li><a href="<?php echo APP_URL ?>">売却物件相談</a></li>
			<li class="SP"><a href="<?php echo APP_URL ?>">個人情報保護方針</a></li>
			<li class="SP"><a href="<?php echo APP_URL ?>">サイトマップ</a></li>
		</ul>
		<div class="col4 PC">
			<p class="text1"><a href="javascript:void(0)" rel="<?php echo APP_URL ?>"><img src="<?php echo APP_ASSETS ?>img/common/logo.svg" width="140" height="30" alt="COMPANY"></a><span>お客様のライフスタイルにあった<br>
たったひとつの家をつくる</span></p>
			<p class="text2">〒000-0000<br>
名古屋市中村区則武1丁目7番13号</p>
			<p class="text3"><img src="<?php echo APP_ASSETS ?>img/common/icon/ico_phone.svg" alt="" width="13" height="17"><span class="fPhone">0000-000-000</span><span class="fTime">営業時間 9：00〜18：00<br>
定休日 水曜日・第2第3火曜日</span></p>
			<p class="text4"><a href="<?php echo APP_URL ?>contact/" class="opa" style="opacity: 1;">Contact us</a></p>
			<ul>
				<li><a href="<?php echo APP_URL ?>">個人情報保護方針</a> | </li>
				<li><a href="<?php echo APP_URL ?>">サイトマップ</a></li>
			</ul>
		</div>
	</div>
	<p class="taC SP"><a href="javascript:void(0)" rel="<?php echo APP_URL ?>"><img src="<?php echo APP_ASSETS ?>img/common/logo.svg" width="157" height="33" alt="COMPANY"></a></p>
	<div class="copyright"><span>Copyright © COMPANY Co. Ltd. <br class="SP">All rights reserved.</span></div>
</footer>

<script src="<?php echo APP_ASSETS; ?>js/lib/jquery1-12-4.min.js"></script>
<script src="<?php echo APP_ASSETS; ?>js/lib/smoothscroll.min.js"></script>
<script src="<?php echo APP_ASSETS; ?>js/lib/biggerlink.min.js"></script>
<script src="<?php echo APP_ASSETS; ?>js/common.min.js"></script>
<script src="<?php echo APP_ASSETS; ?>js/functions.min.js"></script>
