<table class="tableContact" cellspacing="0">
  <tr>
    <th><em class="required">【必須】</em>会社名</th>
    <td>
      <input type="text" name="company" placeholder="例) ◯△×株式会社" class="validate[required]">
    </td>
  </tr>
  <tr>
    <th><em class="required">【必須】</em>担当者名</th>
    <td>
      <input type="text" name="nameuser" placeholder="例) 山田 太郎" class="validate[required]">
    </td>
  </tr>
  <tr>
    <th><em class="required">【必須】</em>電話番号</th>
    <td>
      <input type="text" name="tel" placeholder="例) 1234567" class="validate[required]">
    </td>
  </tr>
  <tr>
    <th><em class="required">【必須】</em>メールアドレス</th>
    <td>
      <input type="url" name="email" placeholder="例) XXXX@sample.co.jp" class="validate[required]">
    </td>
  </tr>
  <tr>
    <th>【任意】お問い合わせ内容</th>
    <td>
      <textarea name="content" id="" cols="30" rows="10"></textarea>
    </td>
  </tr>
</table>