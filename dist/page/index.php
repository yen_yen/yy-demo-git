<?php
$thisPageName = 'page';
include_once(dirname(__DIR__) . '/app_config.php');
include(APP_PATH.'libs/head.php');
?>
<link rel="stylesheet" href="<?php echo APP_ASSETS ?>css/jquery-ui.css">
<link rel="stylesheet" href="<?php echo APP_ASSETS ?>css/page/page.min.css">
</head>
<body id="page" class='page'>
<!-- HEADER -->
<?php include(APP_PATH.'libs/header.php'); ?>
<div id="wrap">
	<main>
		<div class="container">
			<br><br><br><br>
			<h2>Range Slider</h2>
			<br><br>
			<div class="slider-range slider-range-year">
				<h3 class="range-ttl">Year</h3>
				<div class="slider-range-wrap">
					<p class="range-txt txt-min">
						<input type="text" id="minyear"  readonly>
					</p>
					<p class="range-txt txt-max">
						<input type="text" id="maxyear"  readonly>
					</p>
					<div id="slider-range-year"></div>
				</div>
			</div>
			<br><br>
			<div class="slider-range slider-range-price">
				<h3 class="range-ttl">Price</h3>
				<div class="slider-range-wrap">
					<p class="range-txt txt-min">
						<input type="text" id="minprice"  readonly>
					</p>
					<p class="range-txt txt-max">
						<input type="text" id="maxprice"  readonly>
					</p>
					<div id="slider-range-price"></div>
				</div>
			</div>
			<br><br>
			<div class="slider-range slider-range-displacement">
				<h3 class="range-ttl">Displacement</h3>
				<div class="slider-range-wrap">
					<p class="range-txt txt-min">
						<input type="text" id="mindisplacement"  readonly>
					</p>
					<p class="range-txt txt-max">
						<input type="text" id="maxdisplacement"  readonly>
					</p>
					<div id="slider-range-displacement"></div>
				</div>
			</div>
		</div>
		
	</main>
</div><!-- #wrap -->
<!-- FOOTER -->
<?php include(APP_PATH.'libs/footer.php'); ?>
<script src="<?php echo APP_ASSETS ?>js/lib/jquery-ui.js"></script>
<script>
	function formatNumber (num) {
	    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
	}

	$(function() {
		$( "#slider-range-year" ).slider({
			range: true,
			min: 1990,
			max: 2019,
			values: [ 0, 2019 ],
			slide: function( event, ui ) {
				$( "#minyear" ).val( "〜" + ui.values[ 0 ]);
				$( "#maxyear" ).val(ui.values[ 1 ] + "〜" );
			}
		});
		$( "#minyear" ).val( "〜" + $( "#slider-range-year" ).slider( "values", 0 ));
		$( "#maxyear" ).val($( "#slider-range-year" ).slider( "values", 1 ) + "〜");

		$( "#slider-range-price" ).slider({
			range: true,
			min: 50000,
			max: 5000000,
			values: [ 50000, 5000000 ],
			slide: function( event, ui ) {
				$( "#minprice" ).val( "〜¥" + formatNumber(ui.values[ 0 ]) );
				$( "#maxprice" ).val("¥" + formatNumber(ui.values[ 1 ]) + "〜" );
			}
		});
		$( "#minprice" ).val( "〜¥" + formatNumber($( "#slider-range-price" ).slider( "values", 0 )));
		$( "#maxprice" ).val("¥" + formatNumber($( "#slider-range-price" ).slider( "values", 1 )) + "〜");


		$( "#slider-range-displacement" ).slider({
			range: true,
			min: 550,
			max: 6000,
			values: [ 550, 6000 ],
			slide: function( event, ui ) {
				$( "#mindisplacement" ).val( "〜" + ui.values[ 0 ] + "cc");
				$( "#maxdisplacement" ).val(ui.values[ 1 ]+ "cc〜" );
			}
		});
		$( "#mindisplacement" ).val("〜" + $( "#slider-range-displacement" ).slider( "values", 0 ) + "cc");
		$( "#maxdisplacement" ).val($( "#slider-range-displacement" ).slider( "values", 1 ) + "cc〜");

	});
</script>
</body>
</html>

