<?php
function my_custom_ggmap_post_type() {
  register_post_type('ggmap', array (
    'labels'                  => array (
      'name'                  => __( '地図' ),
      'singular_name'         => __( '地図' ),
      'add_new'               => __( '新しく地図を書く' ),
      'add_new_item'          => __( '地図記事を書く' ),
      'edit_item'             => __( '地図記事を編集' ),
      'new_item'              => __( '新しい地図記事' ),
      'view_item'             => __( '地図記事を見る' ),
      'search_staff'          => __( '地図記事を探す' ),
      'not_found'             => __( '地図記事はありません' ),
      'not_found_in_trash'    => __( 'ゴミ箱に地図記事はありません' ),
      'parent_item_colon'     => ''
    ),
    'public'                  => true,
    'rewrite'                 => true,
    'show_ui'                 => true,
    'supports'                => array ( 'title', 'editor', 'thumbnail', 'revisions' ),
    'query_var'               => true,
    'menu_icon'               => 'dashicons-welcome-write-blog',
    'has_archive'             => true,
    'hierarchical'            => false,
    'menu_position'           => 5,
    'capability_type'         => 'post',
    'show_in_admin_bar'       => true,
    'publicly_queryable'      => true,
  ));
}
add_action ( 'init', 'my_custom_ggmap_post_type' );