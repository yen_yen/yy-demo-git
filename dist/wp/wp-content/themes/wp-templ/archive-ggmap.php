<?php
$thisPageName = 'ggmap';
include(APP_PATH.'libs/head.php');
  $param = array(
    'post_type'      => 'ggmap',
    'order'          => 'DESC',
    'posts_per_page' => -1,
    'post_status'    => 'publish'
  );
  $contentmap = new WP_Query($param);
  if ($contentmap->have_posts()) :
    while($contentmap->have_posts()) : $contentmap->the_post();
    $map_ID    		= 'map-'.get_the_ID();
    $map_title    = get_the_title();
    $map_mainimg  = get_field('mod_mainimg');
    $map_markers  = get_field('img_markers');
    $markersURL   = $map_markers['url'];
    $map_addess 	= get_field('gg_addess');
    $map_latlong  = get_field('mod_ggmap_latlong');
    $map_lat 			= $ggmap_latlong['lat'];
    $map_lng 			= $ggmap_latlong['lng'];
    $mapURL       = $map_mainimg['url'];

    $photo        = ($modURL) ? $modURL : get_first_image($mod_content);

    $contentMap[] = array(
      'content' => '<div class="inf-onmap"><div class="inf-onmap-img"><img src="'.$photo.'" /></div><div class="inf-onmap-txt"><p class="ttl">'.get_the_title().'</strong></p><p class="txt">'.preg_replace('/\r\n|\n|\r/','',strip_tags($mod_address)).'</p></div></div>',
      'lat' => ''.$add_lat.'',
      'long' => ''.$add_lng.'',
      'anchor' => ''.$mod_ID.'',
      'icon' => $mapURL,
    );

  endwhile;
  endif;
?>
<link rel="stylesheet" href="<?php echo APP_ASSETS; ?>css/page/ggapi.min.css">
</head>
<body id="map" class="p-map">
<!-- HEADER -->
<?php include(APP_PATH.'libs/header.php'); ?>
<div id="wrap">
  <main>
    <div class="cmn-keyvisual">
      <div class="keyvisual-inner">
        <p class="text-en">MAP</p>
        <h1 class="text-ja">地図</h1>
      </div>
    </div>
    <div class="wcm">
      <div class="api-map">
        <div id="gmap2" class=""></div>
      </div>
    </div>
  </main>
</div>
<!-- FOOTER -->
<?php include(APP_PATH.'libs/footer.php'); ?>
<script async defer src='https://maps.googleapis.com/maps/api/js?&key=AIzaSyBpc_31ry-iedXIQab1xKrjEYw91Q74_Uw'></script>
<script>
  function initialize() {
    var markers = new Array();
    var stylez = [{featureType: "all",elementType: "all",stylers: [{ saturation: -100 } ]}];

    var mapOptions = {
	    zoom: 13,
	    mapTypeId: google.maps.MapTypeId.ROADMAP,
	    center: new google.maps.LatLng(35.5306689, 139.5775654),
	    navigationControl: true,
	    mapTypeControl: true,
	    scaleControl: true,
	  };

    var locations = [
      <?php
        foreach ($contentMap as $k => $v) {
          if(!empty($v['lat']) && !empty($v['long']))
            echo "[new google.maps.LatLng({$v['lat']}, {$v['long']}), '{$v['content']}', '{$v['icon']}', '{$v['anchor']}'],";
        }
      ?>
    ];

    var map = new google.maps.Map(document.getElementById('gmap2'), mapOptions);
    var mapType = new google.maps.StyledMapType(stylez, { name:"Grayscale" });
    map.mapTypes.set('tehgrayz', mapType);
    map.setMapTypeId('tehgrayz');

    var infowindow = new google.maps.InfoWindow();

    for (var i = 0; i < locations.length; i++) {
        // Append a link to the markers DIV for each marker
        // $('#markers').append('<a class="marker-link" data-markerid="' + i + '" href="#'+locations[i][3]+'"></a> ');

        var marker = new google.maps.Marker({
            position: locations[i][0],
            map: map,
            icon: locations[i][2],
        });

        // Register a click event listener on the marker to display the corresponding infowindow content
        google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
            return function () {
                infowindow.setContent(locations[i][1]);
                infowindow.open(map, marker);
            }
        })(marker, i));

        google.maps.event.addListener(marker, 'click', (function (marker, i) {
          return function () {
            $('html,body').animate({ scrollTop: $("#"+locations[i][3]).offset().top - 146},'slow');
            if(window.innerWidth < 767) {
          $('html,body').animate({ scrollTop: $("#"+locations[i][3]).offset().top - 60},'slow');
        }
          }
        })(marker, i));

        // Add marker to markers array
        markers.push(marker);
    }
  }
</script>
</body>
</html>
